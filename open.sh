#!/bin/bash
#check address
if [ -z "$1" ]
then
	echo "No address supplied"
	exit 1
fi
#check port
if [ -z "$2" ]
then
	echo "No port supplied"
	exit 2
fi
#check argument
if [ -z "$3" ]
then
	echo "No command supplied"
	exit 3
fi
case $3 in
# open spotify app
  start)
    arg="su -c \"am start -n com.spotify.music/.MainActivity\""
    ;;
#start music
  play)
    arg="su -c \"input keyevent 126\""
    ;;
#stop music
  stop)
    arg="su -c \"input keyevent 127\""
    ;;
#increase volume
  up)
    arg="su -c \"input keyevent 24\""
    ;;
#decrease volume
  down)
    arg="su -c \"input keyevent 25\""
    ;;
  *)
    echo -n "unknown command"
    exit 5
    ;;
esac
ssh $1 -p $2 "$arg"

if [ $? -ne 0 ]
then
	echo "USAGE: $0 <address> <port>"
	exit 4
fi

exit 0